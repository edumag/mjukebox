# mJukebox

Connect your spotify with your clients or friends.

<img src="./resource/jukebox.png" width="450"/>

An easy way to allow songs to be added to the play queue.

## Requirements

To be able to interact with your spotify it is necessary to create an App at https://developer.spotify.com/

## Install

### composer

```
composer install
cp config-example.php storage/config.php
```

### Config

Once created the app, we will have our CLIENT_ID and SECRET_ID that we must add in the storage/config.php.

And setting the app with your domain.

![](./dev/setting.png)



## Iniciar sesión

To log in to mjukebox you need to go to /admin, the first time it will ask you to give it the necessary permissions for it to work.



![](/home/edu/desarrollo/mjukebox/dev/permissions.png)



That's all...
