<?php
include '../functions/db.php';
require '../vendor/autoload.php';
include '../storage/config.php';

$db = sqlite_open('../storage/mjukebox.sqlite');

$sql = "SELECT access_token, token_type, state, expires_in FROM authorize ORDER BY id desc LIMIT 1";
$result_raw = sqlite_query($db, $sql);
$result = sqlite_fetch_array($result_raw, False);

if ( ! $result ) {
  header('Content-Type: application/json'); 
  echo json_encode(['error' => 'without session']);
  exit();
}

$accessToken = $result['access_token'];
$refreshToken = $result['expires_in'];

$session = new SpotifyWebAPI\Session(
  $CLIENT_ID,
  $SECRET_ID,
  $CALLBACK_URL
);

if ($accessToken) {
    $session->setAccessToken($accessToken);
    $session->setRefreshToken($refreshToken);
} else {
    $session->refreshAccessToken($refreshToken);
}

$options = [
    'auto_refresh' => true,
];

$api = new SpotifyWebAPI\SpotifyWebAPI($options, $session);

$api->setSession($session);

try {


  if ( isset($_GET['search']) ) {
    $search_raw = $api->search($_GET['search'], 'track', False);
    $result = $search_raw->tracks->items;
    if ( $result ) {
      $data = ['result' => $result]; 
      header('Content-Type: application/json'); 
      echo json_encode($data);
      // header('Location: index.php');
      exit();
      }
    }

} catch (Exception $e) {
  header('Content-Type: application/json'); 
  echo json_encode(['error' => 'without session']);
  exit();
}
?>
