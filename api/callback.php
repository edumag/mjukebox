<?php
require '../vendor/autoload.php';
require '../functions/db.php';
require '../storage/config.php';

if ( isset($_GET['code']) ) {

  $state = $_GET['state'];
  $code =  $_GET['code'];

} else {

  echo "Sese token";
  exit();

}

// Request a access token using the code from Spotify
$session = new SpotifyWebAPI\Session(
  $CLIENT_ID,
  $SECRET_ID,
  $CALLBACK_URL
);

$session->requestAccessToken($_GET['code']);
$accessToken = $session->getAccessToken();
$scopes = $session->getScope();
$refreshToken = $session->getRefreshToken();

// Store the access and refresh tokens somewhere. In a session for example
$db = sqlite_open('../storage/mjukebox.sqlite');
$entry_sql = "INSERT INTO authorize VALUES (null, '" . $accessToken . "', '" . implode(',', $scopes) . "', '" . $refreshToken . "', '".$state."');";
sqlite_query($db, $entry_sql);

// Send the user along and fetch some data!
header('Location: /index.php');
die();
?>
