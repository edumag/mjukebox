# mJukebox develop

## Install local server.

$ ddev config

```
Creating a new ddev project config in the current directory (/home/edu/desarrollo/mjukebox) 
Once completed, your configuration will be written to /home/edu/desarrollo/mjukebox/.ddev/config.yaml
 
Project name (mjukebox): 

The docroot is the directory from which your site is served.
This is a relative path from your project root at /home/edu/desarrollo/mjukebox 
You may leave this value blank if your site files are in the project root 
Docroot Location (current directory): 
Found a php codebase at /home/edu/desarrollo/mjukebox. 
Project Type [backdrop, drupal10, drupal6, drupal7, drupal8, drupal9, laravel, magento, magento2, php, shopware6, typo3, wordpress] (php): php
Project type has no settings paths configured, so not creating settings file. 
Configuration complete. You may now run 'ddev start'. 
```

ddev start

```
Successfully started mjukebox 
Project can be reached at https://mjukebox.ddev.site:4443 https://127.0.0.1:49170 
```

## Install in server with dokku

## Server
```
dokku apps:create mjukebox
dokku domains:add mjukebox.lesolivex.com
dokku domains:add mjukebox mjukebox.lesolivex.com
mkdir /var/lib/dokku/data/storage/mjukebox
sudo chmod -R 755 /var/lib/dokku/data/storage/mjukebox/
sudo chown -R dokku:dokku /var/lib/dokku/data/storage/mjukebox
dokku storage:mount mjukebox /var/lib/dokku/data/storage/mjukebox:/app/storage
dokku config:set --no-restart mjukebox DOKKU_LETSENCRYPT_EMAIL=edu@lesolivex.com
dokku letsencrypt mjukebox
dokku letsencrypt:auto-renew
dokku letsencrypt:cron-job --add
```

### Local.

```
git clone git@gitlab.com:edumag/mjukebox.git
git remote add dokku dokku@lesolivex:mjukebox
git push dokku
```
