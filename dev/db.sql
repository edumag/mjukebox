CREATE TABLE authorize
            (
            id INTEGER PRIMARY KEY,
            access_token text,
            token_type text,
            expires_in text,
            state text
            );

