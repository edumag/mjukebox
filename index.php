<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" href="./resource/mjukebox.png" />
    <link rel="stylesheet" type="text/css" href="./mjukebox.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>mJukebox</title>
  </head>
  <body>
  <div id="waiting">
    <div class="lds-ripple"><div></div><div></div></div>
  </div>
  <div id="app">
    <main>

      <div class="item_box">
        <h2>Ara sona</h2>
        <div id="current_play"></div>
      </div>

      <div class="item_box">
        <h2>En cua...</h2>
        <a id="previous" onclick="previousPage()">
          <img src="./resource/up.png"/>
        </a>
        <div id="queue_box"></div>
        <a id="next" onclick="nextPage()">
          <img src="./resource/down.png"/>
        </a>
      </div>

      <!-- input search -->
      <div id="input_search_box" class="input_box">
        <input placeholder="Que vols escoltar?" id="search_input" name="search_input"/>
        <span id="cancel_search"  href="#" onclick="clear_input_search()"><img width="20" src="resource/cancel.png"/></span>
      </div>

      <div id="notify_box" class="notify hide"></div>

      <div id="item_box_search" class="item_box">
        <a id="previousSearch" onclick="previousPage('search')">
          <img src="./resource/up.png"/>
        </a>
        <div id="search_box"></div>
        <a id="nextSearch" onclick="nextPage('search')">
          <img src="./resource/down.png"/>
        </a>
      </div>
        
      </div>

    </main>
  </div>
  </body>
  <script>

    let own_songs = <?php echo isset($_SESSION['tracks']) ? json_encode($_SESSION['tracks']) : '[]'; ?>;

    console.log({own_songs: own_songs});

    let session = null;
    let current_play = null;
    let queue = null;
    let max_items = 2;
    let search_length = 0;

    let list = new Array();
    let pageList = new Array();
    let currentPage = 1;
    let numberPerPage = 3;
    let numberOfPages = 1;   // calculates the total number of pages

    let listSearch = new Array();
    let pageListSearch = new Array();
    let currentPageSearch = 1;
    let numberPerPageSearch = 3;
    let numberOfPagesSearch = 1;   // calculates the total number of pages

    function clear_input_search() {
      let i = document.getElementById('search_input');
      i.value = '';
      init_list_search();
    }

    function verify_queue(id) {

      if ( verify_own_song(id) ) {
        add_notify('Aquesta cançó ja va ser afegida.');
        return false;
      }

      console.log('verify: ' + id);
      console.log({queue: list});
      if ( id == current_play.id ) {
        add_notify('Cançó reproduint-se en aquests moments.');
        return false
      }
      for (i = 0; i < list.length; i++) {
        var list_id = list[i].id;
        console.log('id: ' + id + ' list_id: ' + list_id + 'cp: ' + current_play.id);
        if ( id == list_id ) {
          add_notify('Cançó repetida, no afegim a la cua.');
          return false
        }
      }
      return true;
    }

    function verify_own_song(id) {
      for (i = 0; i < own_songs.length; i++) {
        var list_id = own_songs[i].id;
        if ( id == list_id ) {
          console.log('Coincide con canción propia añadida');
          return true
        }
      }
    }

    function del_queue(id) {
      console.log('@todo del_queue: ' + id);
    }

    function add_queue(id) {

      if ( ! verify_queue(id) ) {
        return true;
      }
      document.getElementById('waiting').style.display = 'block';
      // Creating Our XMLHttpRequest object 
      var xhr = new XMLHttpRequest();

      // Making our connection  
      var url = 'api/add_queue.php?id=' + id;
      xhr.open("GET", url, true);

      // function execute after request is successful 
      xhr.onreadystatechange = function () {
          if (this.readyState == 4 && this.status == 200) {
              add_notify('Cançó afegida a la cua.');
              let now = Date.now();
              own_songs.push({id: id, time: now});
              pageList.push({id: id});
              currentPage = 1;
              makeList();
              console.log({own_songs: own_songs});
              document.getElementById('waiting').style.display = 'none';

          }
      }
      // Sending our request 
      xhr.send();
    }

    function init_list_search() {
      document.getElementById('item_box_search').style.display = 'none';
      // document.getElementById('cancel_search').style.display = 'none';
      listSearch = new Array();
      pageListSearch = new Array();
      currentPageSearch = 1;
      numberPerPageSearch = 3;
      numberOfPagesSearch = 1;   // calculates the total number of pages
    }

    function search(string) {

      console.log('Search: ' + string);

        // Creating Our XMLHttpRequest object 
        var xhr = new XMLHttpRequest();

        // Making our connection  
        var url = 'api/search.php?search=' + string;
        xhr.open("GET", url, true);

        // function execute after request is successful 
        xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('search_box').innerHTML = '';
                init_list_search();
                var data = JSON.parse(this.responseText);

                for (let i of data['result']) { 
                  console.log(i);
                  listSearch.push(i);
                }
                numberOfPagesSearch = getNumberOfPages('search');
                loadList('search');


            }
        }
        // Sending our request 
        xhr.send();
    }

    function drawListSearch() {

      document.getElementById('item_box_search').style.display = 'block';
      document.getElementById("search_box").innerHTML = "";

      for (i = 0; i < pageListSearch.length; i++) {

        console.log({i: pageListSearch[i]});

        var list = document.createElement("div");
        list.classList.add('title');
        var a = document.createElement("a");
        var id = pageListSearch[i].id;
        a.onclick = function() {
          add_queue(this.id);
          };
        a.href="#" + pageListSearch[i].id;
        a.id = id;
        list.appendChild(a);
        var img = document.createElement("img");
        img.src = pageListSearch[i].album.images[2].url;
        a.appendChild(img);
        var span = document.createElement("span");
        span.innerHTML = pageListSearch[i].name;
        a.appendChild(span);
        var p = document.createElement("p");
        p.innerHTML = pageListSearch[i].artists[0].name;
        span.appendChild(p);
        document.getElementById('search_box').appendChild(list);
      }

    }

    function search_change(string) {
      console.log({anterior: search_length, actual: string.length})
      if ( string == undefined ) {
        console.log('undefined');
        search_length = 0;
        return;
      }
      if ( string.length > 2 && string.length > search_length ) {
        search(string);
      } else if ( string.length < search_length ) {
        console.log('Clean search');
        init_list_search();
      }
      search_length = string.length;
      document.getElementById('cancel_search').style.display = 'inline';
      return false;

    }

    let input_search = document.getElementById('search_input');
      input_search.addEventListener('input', function() {
        search_change(this.value)
      }
    );

    function add_notify(msg) {
      let notify_box = document.getElementById('notify_box');
      notify_box.innerHTML = '';
      notify_box.classList.remove('hide');
      notify_box.classList.add('show');
      var span = document.createElement("span");
      span.innerHTML = msg;
      notify_box.appendChild(span);
      setTimeout(function(){
        del_notify();
      }, 7000);
    }

    function del_notify() {
      let notify_box = document.getElementById('notify_box');
      notify_box.classList.remove('show');
      notify_box.classList.add('hide');
      // notify_box.innerHTML = '';
    }

    function makeList() {

      var xhr = new XMLHttpRequest();
      var url = 'api/queue.php';
      xhr.open("GET", url, true);


      xhr.onreadystatechange = function () {

        if (this.readyState == 4 && this.status == 200) {

          var data = JSON.parse(this.responseText);

          if ('error' in data) {
            console.error(data['error']);
            add_notify('Sense sessió');
            check();
            return false;
          } else {
            session = true;
          }

          current_play = data['currently_playing'];

          list = new Array();
          for (let i of data['queue']) { 
            list.push(i);
          }
          console.log({NewList: list});
          numberOfPages = getNumberOfPages();
          loadList();
        }
      }

      // Sending our request 
      xhr.send();
    }

  function load() {
    makeList();
  }

  function getNumberOfPages(list_type) {
      if ( list_type == 'search' ) {
        return Math.ceil(listSearch.length / numberPerPageSearch);
      } else {
        return Math.ceil(list.length / numberPerPage);
      }
  }

  function nextPage(list_type) {
    if ( list_type == 'search' ) {
      currentPageSearch += 1;
      loadList('search');
    } else {
      currentPage += 1;
      loadList();
    }
  }

  function previousPage(list_type) {
    if ( list_type == 'search' ) {
      currentPageSearch -= 1;
      loadList('search');
    } else {
      currentPage -= 1;
      loadList();
    }
  }

  function firstPage() {
      currentPage = 1;
      loadList();
  }

  function lastPage() {
      currentPage = numberOfPages;
      loadList();
  }

  function loadList(list_type) {
    if ( list_type == 'search') {
      var begin = ((currentPageSearch - 1) * numberPerPageSearch);
      var end = begin + numberPerPageSearch;
      pageListSearch = listSearch.slice(begin, end);
      drawListSearch();    // draws out our data
      check();       // determines the states of the pagination buttons
    } else {
      var begin = ((currentPage - 1) * numberPerPage);
      var end = begin + numberPerPage;
      pageList = list.slice(begin, end);
      drawList();    // draws out our data
      check();       // determines the states of the pagination buttons
    }
  }

  function drawList() {

    // Update current play.
    document.getElementById('current_play').innerHTML = '';
    var i = current_play;

    var list = document.createElement("div");
    list.classList.add('title');
    var img = document.createElement("img");
    img.src = i.album.images[2].url;
    list.appendChild(img);
    var span = document.createElement("span");
    span.innerHTML = i.name;
    list.appendChild(span);
    var p = document.createElement("p");
    p.innerHTML = i.artists[0].name;
    span.appendChild(p);
    document.getElementById('current_play').appendChild(list);

    document.getElementById("queue_box").innerHTML = "";
    for (i = 0; i < pageList.length; i++) {

      var listbox = document.createElement("div");
      listbox.classList.add('title');
      var img = document.createElement("img");
      img.src = pageList[i]['album'].images[2].url;
      listbox.appendChild(img);
      var span = document.createElement("span");
      span.innerHTML = pageList[i].name;
      listbox.appendChild(span);
      var p = document.createElement("p");
      p.innerHTML = pageList[i].artists[0].name;
      span.appendChild(p);
      // @todo Falta función del_queue() en API.
      // if ( verify_own_song(pageList[i].id) ) {
      //   console.log('yes: ' + pageList[i].id);
      //   var a = document.createElement("a");
      //   var id = pageList[i].id;
      //   a.onclick = function() {
      //     del_queue(this.id);
      //     };
      //   a.href="#" + pageList[i].id;
      //   var span = document.createElement("span");
      //   span.innerHTML = 'X';
      //   a.setAttribute('class', 'cancel');
      //   a.appendChild(span);
      //   listbox.appendChild(a);
      // }
      document.getElementById('queue_box').appendChild(listbox);

      // document.getElementById("list").innerHTML += '<div>' + pageList[r] + "</div>";
    }

  }

  function check() {
      // console.log({currentPage: currentPage, numberOfPages: numberOfPages})
      if ( currentPage == numberOfPages ) {
        document.getElementById('next').style.display = 'none';
      } else {
        document.getElementById('next').style.display = 'block';
      }
      if ( currentPage == 1 ) {
        document.getElementById('previous').style.display = 'none';
      } else {
        document.getElementById('previous').style.display = 'block';
      }
      if ( currentPageSearch == numberOfPagesSearch ) {
        document.getElementById('nextSearch').style.display = 'none';
      } else {
        document.getElementById('nextSearch').style.display = 'block';
      }
      if ( currentPageSearch == 1 ) {
        document.getElementById('previousSearch').style.display = 'none';
      } else {
        document.getElementById('previousSearch').style.display = 'block';
      }
      // if ( ! session ) {
      //   console.log('Sense sessió'); 
      //   document.getElementById('item_box_search').style.display = 'none';
      // }
      if ( listSearch.length < 1 ) {
        document.getElementById('item_box_search').style.display = 'none';
      }
  }
  document.getElementById('next').style.display = 'none';
  document.getElementById('previous').style.display = 'none';
  document.getElementById('nextSearch').style.display = 'none';
  document.getElementById('previousSearch').style.display = 'none';
  document.getElementById('item_box_search').style.display = 'none';
  load();
  const interval = setInterval(load, 5000);

  </script>
</html>
