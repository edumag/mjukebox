<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" href="../mjukebox.svg" />
    <link rel="stylesheet" type="text/css" href="../mjukebox.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Esplet</title>
  </head>
  <body>
    <div id="app">
      <?php
      include '../functions/db.php';
      require '../vendor/autoload.php';
      include '../storage/config.php';

      $db = sqlite_open('../storage/mjukebox.sqlite');

      // Fetch the saved access token from somewhere. A session for example.
      $sql = "SELECT access_token, token_type, state FROM authorize ORDER BY id desc LIMIT 1";
      $result_raw = sqlite_query($db, $sql);
      $result = sqlite_fetch_array($result_raw, False);

      $accessToken = $result['access_token'];

      $api = new SpotifyWebAPI\SpotifyWebAPI();

      try {
        $api->setAccessToken($accessToken);
      } catch (Exception $e) {
        echo "Exception: ";
        echo "<pre>"; print_r($e); echo "</pre>";
      }

      // if (! $api->setAccessToken($accessToken) ) {
      //   header('Location: login.php');
      //   die();
      // }

      // Getting Spotify catalog data is of course also possible
      // print_r(
      //     // $api->getMyCurrentTrack()
      //     $api->getMyQueue()
      // );
      try {
        $current_play = $api->getMyCurrentTrack();
      } catch (Exception $e) {
        header('Location: login.php');
        die();
        echo "Exception: ";
        echo "<pre>"; print_r($e); echo "</pre>";
      }

      $queue = $api->getMyQueue(); 

      $search = False;
      if ( isset($_GET['search']) ) {
        $search_raw = $api->search($_GET['search'], 'track', False);
        $search = $search_raw->tracks->items;
      }
      ?>
  <main>
    <div class="bg-white">
      <h1 class="text-green-500 text-3xl italic text-center py-2 font-bold">Admin</h1>
    </div>
    <div id="admin_box">
      <p><h2>Estat de sessió</h2></p>
      <p><h2>Usuaris connectats</h2></p>
      <p><h2>Tancar sessió</h2></p>
    </div>

  </main>
    </div>
  </body>
<style>
#admin_box {
  color: white;
  text-align: center;
}
</style>
</html>
