<?php
require '../vendor/autoload.php';
include '../storage/config.php';

$session = new SpotifyWebAPI\Session(
  $CLIENT_ID,
  $SECRET_ID,
  $CALLBACK_URL
);
$state = $session->generateState();
$options = [
    'scope' => [
        'playlist-read-private',
        'user-read-private',
        'user-modify-playback-state',
        'app-remote-control',
        'user-read-playback-state',
    ],
    'state' => $state,
];

header('Location: ' . $session->getAuthorizeUrl($options));
die();
?>
